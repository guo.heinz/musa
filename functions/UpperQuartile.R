#Tabletest is a dataframe with numerical features, no NaN/Inf
#rows: patients
#Columns: features/counts

#Tabletest <- as.data.frame(t(prot_feat2[complete.cases(prot_feat2), ]))
  
UpperQuartile<-function(Tabletest){
  #' Upper-quartile (UQ) scaling normalization wrapper function
  #@importFrom edgeR calcNormFactors
  #@details SCONE scaling wrapper for \code{\link[edgeR]{calcNormFactors}}).
  #@export
  #@param ei Numerical matrix. (rows = genes, cols = samples).
  #@return UQ normalized matrix.
  
  #libraries
  library(scone)
  library(edgeR)

  Column=ncol(Tabletest)
  namescol=names(Tabletest)
  
    
  #removing genes/features that are zero in all patients
  x <- colSums(Tabletest==0)!=nrow(Tabletest)
  newCountTable <- Tabletest[,x]
  
  #Upper Quartile Normalization---------------------------------------------------------------
  ei=as.matrix(t(newCountTable))
  size_fac = calcNormFactors(ei, method = "upperquartile")
  scales = (colSums(ei) * size_fac)
  eo = t(t(ei) * mean(scales) / scales)
  
  TableUQ=as.data.frame(t(eo))
  
  # names(TableUQ)=namescol[x] 
  # ColumnUQ=ncol(TableUQ)# Calculate new data size, after removing genes/features that are zero in all patients
  # 
  # #Generate BoxPlots for original and normalized features
  # for (i in 1:ColumnUQ){
  #   
  #   tiff(file = sprintf("Feature %s Upper Quartile.tiff", i), width = 3200, height = 3200, units = "px", res = 300)
  #   
  #   par(mfrow=c(1,2))    # set the plotting area into a 1*2 array
  #   boxplot(newCountTable[, i], xlab=colnames(newCountTable[i]), ylab="Original Values")
  #   boxplot(TableUQ[,i],xlab=colnames(TableUQ[i]), ylab="Normalized Values")
  #   
  #   dev.off()
  # }
  
  return(TableUQ)
}
